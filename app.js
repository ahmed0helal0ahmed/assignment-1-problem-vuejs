const app = Vue.createApp({
  data() {
    return {
      author: {
        name: "Ahmed Helal",
        age: this.calculateAge(1994, 1, 1),
        favoriteNumber: this.getFavoriteNumber(),
      },
      imageLink: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png",
    };
  },
  methods: {
    sayHello() {
      return "hello";
    },
    calculateAge(birthYear, birthMonth, birthDay) {
      const birthDate = new Date(birthYear, birthMonth, birthDay);
      const differInMilliseconds = Date.now() - birthDate.getTime();
      const ageDate = new Date(differInMilliseconds);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    },
    getFavoriteNumber() {
      return Math.random();
    },
  },
});
app.mount("#assignment");
